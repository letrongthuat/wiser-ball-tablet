package com.goodapps.wiserball;

import java.io.File;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.provider.VoicemailContract;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

public class FileFragment extends Fragment {

	private static final String[][] String = null;

	private ListAdapter _fileAdapter;

	private ListView _fileList;
	private ActionMode _actionMode;
	private Button _btnEdit;
	private Button _btnDelete;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_files, container, false);
		String newFile = "";
		makeDir();
		String[] files = getFileList();
		for (int i = 0; i < files.length; i++) {
			newFile = subString(files[i]);
			files[i] = newFile;
		}
		
		_fileAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
				android.R.id.text1, files);
		
		_btnDelete = (Button) v.findViewById(R.id.button_delete_file);
		_btnEdit = (Button) v.findViewById(R.id.button_edit_file);
		
		// Onclick button
		_btnDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(Utils.index >= 0) {
					askForDeletingFile(Utils.index);
				}
			}
		});
		
		_btnEdit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(Utils.index >= 0) {
					renameFile(Utils.index);
				}
			}
		});
		
		_fileList = (ListView) v.findViewById(R.id.list_file);
		_fileList.setAdapter(_fileAdapter);
		_fileList.setOnItemClickListener((OnItemClickListener) getActivity());

		return v;
	}
	
	private String subString(String name) {
		String[] sub = name.split("___");
		String txt1 = "";
		String txt2 = "";
		String txt3 = "";
		String txt4 = "";
		if(sub.length > 2) {
			if(!sub[0].equals("")) {
				txt1 = sub[0].replace("_", "-");
			}
			if(!sub[1].equals("")) {
				txt2 = sub[1].replace("_", ":");
			}
			if(!sub[2].equals("")) {
				txt3 = "[" + sub[2] + "]";
			}
			if(!sub[3].equals("")) {
				txt4 = "[" + sub[3] + "]";
			}
			if(!txt3.equals("") && !txt4.equals("")) {
				name = txt3 + " vs " + txt4 + "\n" + txt1 + " " + txt2;
			} else {
				
			}
		}
		return name;
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Utils.index = -1;
	}



	private String[] getFileList() {
		File folder = new File(Utils.getDataFolder());
		return folder.list();
	}

	private void makeDir() {
		File dir = new File(Utils.getDataFolder());
		if (!dir.exists())
			dir.mkdirs();
	}

	private void renameFile(final int position) {
		File folder = new File(Utils.getDataFolder());
		final String fileName = folder.list()[position];

		AlertDialog alert = DialogFactory.getInputTextDialog(getActivity(),
				R.string.title_rename_file, fileName, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Dialog d = (Dialog) dialog;
						EditText editText = (EditText) d.findViewById(R.id.edit_name);

						String newName = editText.getText().toString();
						if (newName.trim().length() != 0) {
							File from = new File(new File(Utils.getDataFolder()), fileName);
							File to = new File(new File(Utils.getDataFolder()), newName.trim());

							if (!from.renameTo(to)) {
								DialogFactory.showMessageDialog(getActivity(),
										"Can not change the file name.");
								return;
							} else {

								_fileAdapter = new ArrayAdapter<String>(getActivity(),
										android.R.layout.simple_list_item_1, android.R.id.text1,
										getFileList());
								_fileList.setAdapter(_fileAdapter);
							}
						}
					}
				}, null);
		alert.show();
	}

	private void askForDeletingFile(final int position) {
		File folder = new File(Utils.getDataFolder());
		final String fileName = folder.list()[position];

		AlertDialog alert = DialogFactory.getQuestionDialog(getActivity(),
				getString(R.string.title_delete_file),
				getString(R.string.message_delete_file, fileName), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						File file = new File(new File(Utils.getDataFolder()), fileName);

						if (!file.delete()) {
							DialogFactory.showMessageDialog(getActivity(),
									"Can not delete this file.");
							return;
						} else {

							_fileAdapter = new ArrayAdapter<String>(getActivity(),
									android.R.layout.simple_list_item_1, android.R.id.text1,
									getFileList());
							_fileList.setAdapter(_fileAdapter);
						}
					}
				}, null);
		alert.show();
	}
}
