package com.goodapps.wiserball;

import java.io.File;
import java.io.IOException;
import java.text.BreakIterator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.R.bool;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.goodapps.wiserball.data.DbHelper;
import com.goodapps.wiserball.data.PlayerContract;
import com.goodapps.wiserball.data.TeamContract;
import com.goodapps.wiserball.game.WBMatch;
import com.goodapps.wiserball.game.WBPlayer;
import com.goodapps.wiserball.game.WBTeam;
import com.goodapps.wiserball.game.WBTurn;
import com.goodapps.wiserball.type.GameState;
import com.goodapps.wiserball.type.PlayerRole;
import com.goodapps.wiserball.type.PlayerState;
import com.goodapps.wiserball.type.SetupState;
import com.goodapps.wiserball.type.TeamColor;
import com.goodapps.wiserball.type.TurnType;
import com.goodapps.wiserball.widget.PlayerView;

@SuppressLint("SimpleDateFormat") public class MainActivity extends Activity implements OnItemSelectedListener {

	private static final int DEFAULT_TIME_STEP = 10;
	private static final int MIN_COUNT = 10;
	private int _ballNumber;
	private boolean _isWhiteFirst;
	private GameState _gameState;
	private PlayerView _redPlayerViews[];
	private PlayerView _whitePlayerViews[];
	private WBMatch _match;
	private List<WBPlayer> _hitChain;
	private SettingController _settings;
	private int _count;
	private long _duration;
	private DbHelper _dbHelper;
	private boolean _isLess;
	private Button buttonMore;
	private Button btn_select_red;
	private Button btn_select_white;
	private String name_red_team = "";
	private String name_white_team = "";
	private TextView red_team_name, white_team_name;

	private int isSelectTeam = 0;//1 : red , 2: white
	private String txt_red = "";
	private String txt_white = "";
	public static final String EXTRA_SELECT_FILE = "select_file";
	public static final int REQUEST_CODE_SELECT = 100;

	@SuppressLint("HandlerLeak")
	private Handler _countDownHandler = new Handler() {
		@Override
		public void handleMessage(android.os.Message msg) {
			if (_count > 0) {
				_count--;
				((TextView) findViewById(R.id.text_count)).setText(String.valueOf(_count));
			}

			if (_count == 0) {
				// TODO: ask for change turn
				onChangeTurnClicked(null);
			} else {
				_countDownHandler.sendEmptyMessageDelayed(0, 1000);
			}
		};
	};

	@SuppressLint("SimpleDateFormat")
	private TimerTask _timerTask = new TimerTask() {
		@Override
		public void run() {
			MainActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					TextView textTime = (TextView) findViewById(R.id.text_time);
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
					textTime.setText(format.format(new Date()));

					if (_gameState == GameState.Playing || _gameState == GameState.Setupping) {
						_duration++;

						int ss = (int) (_duration % 60);
						int mm = (int) ((_duration / 60) % 60);
						int hh = (int) (_duration / 3600);

						TextView textMatchTime = (TextView) findViewById(R.id.text_match_time);
						textMatchTime.setText(String.format("%02d:%02d:%02d", hh, mm, ss));
					}
				}
			});
		}
	};

	private OnItemSelectedListener _onRedTeamSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
			if (id != -1) {
				List<PlayerContract> players = _dbHelper.getAllPlayersForTeam(id);
				for (int i = 0; i < 7; i++) {
					int playerNumber = _redPlayerViews[i].getPlayerNumber();
					if (playerNumber <= players.size() && playerNumber > 0) {
						_redPlayerViews[i].setPlayerName(players.get(playerNumber - 1).name);
						_redPlayerViews[i]
								.setRole(players.get(playerNumber - 1).role == 1 ? PlayerRole.Captain
										: PlayerRole.Normal);
					} else {
						_redPlayerViews[i].setPlayerName(getString(R.string.default_player_name));
						_redPlayerViews[i].setRole(PlayerRole.Normal);
					}
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) {

		}

	};

	private OnItemSelectedListener _onWhiteTeamSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
			if (id != -1) {
				List<PlayerContract> players = _dbHelper.getAllPlayersForTeam(id);
				for (int i = 0; i < 7; i++) {
					int playerNumber = _whitePlayerViews[i].getPlayerNumber();
					if (playerNumber <= players.size() && playerNumber > 0) {
						_whitePlayerViews[i].setPlayerName(players.get(playerNumber - 1).name);
						_whitePlayerViews[i]
							.setRole(players.get(playerNumber - 1).role == 1 ? PlayerRole.Captain
									: PlayerRole.Normal);
					}
					else {
						_whitePlayerViews[i].setPlayerName(getString(R.string.default_player_name));
						_whitePlayerViews[i].setRole(PlayerRole.Normal);
					}
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) {

		}

	};

	private int _versionCode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		buttonMore = (Button) findViewById(R.id.button_more);
		_isLess = false;
		try {
			_versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
			setTitle(getString(R.string.app_name));
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setContentView(R.layout.activity_main);
		_settings = new SettingController(this);
		_dbHelper = new DbHelper(this);
		// set last ball number
		Spinner spinner = (Spinner) findViewById(R.id.spinner_ball_number);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
				R.array.ball_number, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(this);
		spinner.setSelection(_settings.getBallCount() - 5);

		// set first team
		ToggleButton toggleWhiteFirst = (ToggleButton) findViewById(R.id.toggle_white_first);
		toggleWhiteFirst.setChecked(_settings.isWhiteFirst());
		toggleWhiteFirst.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				_isWhiteFirst = isChecked;
				_settings.setWhiteFirst(isChecked);
			}
		});

		_redPlayerViews = new PlayerView[7];
		_whitePlayerViews = new PlayerView[7];
		_hitChain = new LinkedList<WBPlayer>();

		View redTeam = findViewById(R.id.layout_red_team);
		_redPlayerViews[0] = (PlayerView) redTeam.findViewById(R.id.player_1);
		_redPlayerViews[1] = (PlayerView) redTeam.findViewById(R.id.player_2);
		_redPlayerViews[2] = (PlayerView) redTeam.findViewById(R.id.player_3);
		_redPlayerViews[3] = (PlayerView) redTeam.findViewById(R.id.player_4);
		_redPlayerViews[4] = (PlayerView) redTeam.findViewById(R.id.player_5);
		_redPlayerViews[5] = (PlayerView) redTeam.findViewById(R.id.player_6);
		_redPlayerViews[6] = (PlayerView) redTeam.findViewById(R.id.player_7);

		View whiteTeam = findViewById(R.id.layout_white_team);
		_whitePlayerViews[0] = (PlayerView) whiteTeam.findViewById(R.id.player_1);
		_whitePlayerViews[1] = (PlayerView) whiteTeam.findViewById(R.id.player_2);
		_whitePlayerViews[2] = (PlayerView) whiteTeam.findViewById(R.id.player_3);
		_whitePlayerViews[3] = (PlayerView) whiteTeam.findViewById(R.id.player_4);
		_whitePlayerViews[4] = (PlayerView) whiteTeam.findViewById(R.id.player_5);
		_whitePlayerViews[5] = (PlayerView) whiteTeam.findViewById(R.id.player_6);
		_whitePlayerViews[6] = (PlayerView) whiteTeam.findViewById(R.id.player_7);

		for (int i = 0; i < 7; i++) {
			_redPlayerViews[i].setOnPlayerClickListener(_onPlayerViewClickListener);
			_redPlayerViews[i].setOnSetupSelectListener(this);
			_whitePlayerViews[i].setOnPlayerClickListener(_onPlayerViewClickListener);
			_whitePlayerViews[i].setTeamColor(TeamColor.White);
			_whitePlayerViews[i].setOnSetupSelectListener(this);
		}
		
		btn_select_red = (Button) redTeam.findViewById(R.id.select_red);
		btn_select_white = (Button) whiteTeam.findViewById(R.id.select_white);
		//thuat_
				red_team_name = (TextView)redTeam.findViewById(R.id.red_team_name);
				white_team_name = (TextView)whiteTeam.findViewById(R.id.white_team_name);

		// set onclick button
		btn_select_red.setOnClickListener(_onSelectTeamRed);
		btn_select_white.setOnClickListener(_onSelectTeamWhite);
		
		// reset view
		setGameState(GameState.Closed);

		// timer
		new Timer().scheduleAtFixedRate(_timerTask, 0, 1000);
		
		setLess(_isLess);
		
		String mLanguage = Locale.getDefault().getLanguage();
		
		if(mLanguage.equalsIgnoreCase("en")) {
			updateLanguage(Locale.US);
		} else if(mLanguage.equalsIgnoreCase("vi")) {
			updateLanguage(new Locale("vi"));
		} else if(mLanguage.equalsIgnoreCase("zh")) {
			updateLanguage(Locale.CHINESE);
		}
		
//		updateLanguage(Utils.getLocale(_settings.getLanguage()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		Resources res = Utils.getResourcesForLocale(getResources(), Utils.getLocale(_settings.getLanguage()));
		menu.getItem(0).setTitle(res.getString(R.string.item_teams));
		menu.getItem(1).setTitle(res.getString(R.string.item_files));
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.item_team:
			Intent teamIntent = new Intent(this, TeamActivity.class);
			startActivity(teamIntent);
			break;
		case R.id.item_file:
			Intent fileIntent = new Intent(this, FileActivity.class);
			startActivity(fileIntent);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private OnClickListener _onSelectTeamRed = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Bundle bundel = new Bundle();
			bundel.putBoolean("isButtonSelectTeam", true);
			Intent teamIntent = new Intent(getApplicationContext(), TeamActivity.class);
			teamIntent.putExtras(bundel);
			startActivity(teamIntent);
			//thuatole
			isSelectTeam = 1;
		}
	};
	
	private OnClickListener _onSelectTeamWhite = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Bundle bundel = new Bundle();
			bundel.putBoolean("isButtonSelectTeam", true);
			Intent teamIntent = new Intent(getApplicationContext(), TeamActivity.class);
			teamIntent.putExtras(bundel);
			startActivity(teamIntent);
			//thuatole
			isSelectTeam = 2;
		}
	};

	private OnClickListener _onPlayerViewClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (_gameState != GameState.Playing)
				return;

			PlayerView playerView = (PlayerView) v.getTag();

			if (_hitChain.size() == 0 && !playerView.isSelected()) {
				DialogFactory.showInvalidSideAlert(MainActivity.this, null);
				return;
			}

			WBPlayer player = (WBPlayer) playerView.getTag();

			if (player.getPlayerState() == PlayerState.Out)
				return;

			if (player.getPlayerState() != PlayerState.Green && _hitChain.size() == 0) {
				DialogFactory.showInvalidPlayerStateAlert(MainActivity.this, null);
				return;
			}

			playClickBallSound();
			boolean selected = !v.isSelected();

			if (selected) {
				_hitChain.add(player);
			} else {
				if (_hitChain.indexOf(player) == 0 && _hitChain.size() > 1) {
					DialogFactory.showCantRemoveStaterAlert(MainActivity.this, null);
					return;
				}
				_hitChain.remove(player);
			}

			playerView.getPlayerButton().setSelected(selected);
		}
	};

	private void setupPlayerViewsForBallCount(int ballCount) {
		PlayerView playerViews[][] = new PlayerView[2][];
		playerViews[0] = _redPlayerViews;
		playerViews[1] = _whitePlayerViews;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < ballCount; j++) {
				playerViews[i][j].setVisibility(View.VISIBLE);
			}

			for (int j = ballCount; j < 7; j++) {
				playerViews[i][j].setVisibility(View.GONE);
			}

			playerViews[i][(_ballNumber - 1) / 2].setPlayerNumber(1);

			for (int j = 0; j < (_ballNumber - 1) / 2; j++) {
				playerViews[i][j].setPlayerNumber(j + 2);
			}

			for (int j = (_ballNumber + 1) / 2; j < _ballNumber; j++) {
				playerViews[i][j].setPlayerNumber(j + 1);
			}
		}
	}

	private void resetViewsBeforeMatch() {
		PlayerView playerViews[][] = new PlayerView[2][];
		playerViews[0] = _redPlayerViews;
		playerViews[1] = _whitePlayerViews;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 7; j++) {
				PlayerView playerView = playerViews[i][j];
				playerView.setSelected(false);
				playerView.setPlayerState(PlayerState.Green);
				playerView.setIndiv(0);
				playerView.setHit1Hidden(true);
				playerView.setHit2Hidden(true);
				playerView.setFirstHitReceivedFromTeamString("");
				playerView.setSecondHitReceivedFromTeamString("");
				playerView.getPlayerButton().setEnabled(false);
				playerView.setSetupVisible(false);
				playerView.setSetupEnabled(false);
				playerView.setSetupState(SetupState.Undefined);
				playerView.setRole(PlayerRole.Normal);
				playerView.setPlayerName(getString(R.string.default_player_name));
			}
		}

		((TextView) findViewById(R.id.text_red_fault_turn)).setText(R.string.zero);
		((TextView) findViewById(R.id.text_red_captute_zone)).setText(R.string.zero);
		((TextView) findViewById(R.id.text_white_fault_turn)).setText(R.string.zero);
		((TextView) findViewById(R.id.text_white_captute_zone)).setText(R.string.zero);

		findViewById(R.id.button_red_fault_turn_minus).setEnabled(false);
		findViewById(R.id.button_red_captute_zone_minus).setEnabled(false);
		findViewById(R.id.button_white_fault_turn_minus).setEnabled(false);
		findViewById(R.id.button_white_captute_zone_minus).setEnabled(false);

		((TextView) findViewById(R.id.text_current_turn)).setText(R.string.default_turn);
		((TextView) findViewById(R.id.text_count))
				.setText(String.valueOf(_settings.getCountDown()));
		((TextView) findViewById(R.id.text_match_time)).setText(R.string.default_time);
		findViewById(R.id.button_time_minus).setEnabled(_settings.getCountDown() > MIN_COUNT);

		// adjust red st & stf
		View redTeamLayout = findViewById(R.id.layout_red_team);
		((TextView) redTeamLayout.findViewById(R.id.text_skip_turn)).setText(R.string.zero);
		((TextView) redTeamLayout.findViewById(R.id.text_skip_turn_fault)).setText(R.string.zero);
		((TextView) redTeamLayout.findViewById(R.id.text_score_red)).setText(R.string.zero);

		// adjust white st & stf
		View whiteTeamLayout = findViewById(R.id.layout_white_team);
		((TextView) whiteTeamLayout.findViewById(R.id.text_skip_turn)).setText(R.string.zero);
		((TextView) whiteTeamLayout.findViewById(R.id.text_skip_turn_fault)).setText(R.string.zero);
		((TextView) whiteTeamLayout.findViewById(R.id.text_score_white)).setText(R.string.zero);

		TextView textRedBallRescued = (TextView) findViewById(R.id.text_red_ball_rescued);
		textRedBallRescued.setText("");

		TextView textWhiteBallRescued = (TextView) findViewById(R.id.text_white_ball_rescued);
		textWhiteBallRescued.setText("");

		TextView textRedBall = (TextView) findViewById(R.id.text_red_ball);
		textRedBall.setText("");
		TextView textWhiteBall = (TextView) findViewById(R.id.text_white_ball);
		textWhiteBall.setText("");
	}

	private void updateViewsAfterTurn() {
		PlayerView playerViews[][] = new PlayerView[2][];
		playerViews[0] = _redPlayerViews;
		playerViews[1] = _whitePlayerViews;
		if(_match == null) 
			return;
		if (_match.getCurrentTurnIndex() < _ballNumber * 2)
			setGameState(GameState.Setupping);
		else
			setGameState(GameState.Playing);

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < _ballNumber; j++) {
				PlayerView playerView = playerViews[i][j];

				playerView.setSelected(i == 0 ? !isWhiteTurn() : isWhiteTurn());
				playerView.getPlayerButton().setSelected(false);
				playerView.setControlEnabled(_gameState == GameState.Playing);
				playerView.setSetupVisible(_gameState == GameState.Setupping);

				WBPlayer player = (WBPlayer) playerView.getTag();
				playerView.setSetupEnabled(playerView.isSelected()
						&& playerView.getSetupState() == SetupState.Undefined);

				playerView.setSetupState(player.getSetupState());
				playerView.setPlayerState(player.getPlayerState());
				if(!player.getIsFoul2()) {
					playerView.setFirstHitReceivedFromTeamString(player
							.getFirstHitReceivedFromTeamString());
					playerView.setSecondHitReceivedFromTeamString(player
							.getSecondHitReceivedFromTeamString());
					playerView.setPlayerNumberOfFirstHitFromOpponent(player
							.getPlayerNumberOfFirstHitReceivedFromOpponent());
					playerView.setPlayerNumberOfSecondHitFromOpponent(player
							.getPlayerNumberOfSecondHitReceivedFromOpponent());
					playerView.setFirstHitReceivedFromOpponentIndex(player
							.getFirstHitReceivedFromOpponentIndex());
					playerView.setSecondHitReceivedFromOpponentIndex(player
							.getSecondHitReceivedFromOpponentIndex());
					playerView.setIndiv(player.getIndivCount());
				}
				playerView.setRole(player.getRole());
			}
		}

		// adjust red st & stf
		View redTeamLayout = findViewById(R.id.layout_red_team);
		TextView redST = (TextView) redTeamLayout.findViewById(R.id.text_skip_turn);
		redST.setText(String.valueOf(_match.getRedTeam().getSkipTurn()));
		TextView redSTF = (TextView) redTeamLayout.findViewById(R.id.text_skip_turn_fault);
		redSTF.setText(String.valueOf(_match.getRedTeam().getSkipTurnFault()));
		TextView redScore = (TextView) redTeamLayout.findViewById(R.id.text_score_red);
		redScore.setText(String.valueOf(_match.getRedTeam().getScore()));

		// adjust white st & stf
		View whiteTeamLayout = findViewById(R.id.layout_white_team);
		TextView whiteST = (TextView) whiteTeamLayout.findViewById(R.id.text_skip_turn);
		whiteST.setText(String.valueOf(_match.getWhiteTeam().getSkipTurn()));
		TextView whiteSTF = (TextView) whiteTeamLayout.findViewById(R.id.text_skip_turn_fault);
		whiteSTF.setText(String.valueOf(_match.getWhiteTeam().getSkipTurnFault()));
		TextView whiteScore = (TextView) whiteTeamLayout.findViewById(R.id.text_score_white);
		whiteScore.setText(String.valueOf(_match.getWhiteTeam().getScore()));

		TextView textTurn = (TextView) findViewById(R.id.text_current_turn);

		textTurn.setText(String.format("%d / %d", _match.getCurrentTurnIndex(),
				_match.getTurnCount()));

		findViewById(R.id.button_back).setEnabled(_match.canBack());
		findViewById(R.id.button_next).setEnabled(_match.canNext());

		String mLang = ((Button) findViewById(R.id.button_language)).getText().toString();
		
		TextView textRedBallRescued = (TextView) findViewById(R.id.text_red_ball_rescued);
		textRedBallRescued.setText(_match.getBallRescuedText(_match.getRedTeam(), mLang));

		TextView textWhiteBallRescued = (TextView) findViewById(R.id.text_white_ball_rescued);
		textWhiteBallRescued.setText(_match.getBallRescuedText(_match.getWhiteTeam(), mLang));

		if (_match.getLastTeamColor() == TeamColor.Red) {
			TextView textRedBall = (TextView) findViewById(R.id.text_red_ball);
			textRedBall.setText(_match.getDescriptionOfTurn(_match.getLastTurn(), mLang));
			TextView textWhiteBall = (TextView) findViewById(R.id.text_white_ball);
			textWhiteBall.setText("");
		} else {
			TextView textRedBall = (TextView) findViewById(R.id.text_red_ball);
			textRedBall.setText("");
			TextView textWhiteBall = (TextView) findViewById(R.id.text_white_ball);
			textWhiteBall.setText(_match.getDescriptionOfTurn(_match.getLastTurn(), mLang));
		}
	}

	private boolean isWhiteTurn() {
		if (_match != null && _match.isWhiteTurn())
			return true;
		return false;
	}

	/*
	 * Game Controls
	 */

	public void onNewGameClicked(View view) {
		// Get name 2 team
		String mNameRed = ((TextView)findViewById(R.id.red_team_name)).getText().toString();
		String mNameWhite = ((TextView)findViewById(R.id.white_team_name)).getText().toString();
		// Check name team before start game
		if(!mNameRed.equalsIgnoreCase("") && !mNameWhite.equalsIgnoreCase("")) { 
			_match = new WBMatch(_ballNumber, _settings.isWhiteFirst());
	
			// binding player & player views
			PlayerView playerViews[][] = new PlayerView[2][];
			playerViews[0] = _redPlayerViews;
			playerViews[1] = _whitePlayerViews;
			
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < _ballNumber; j++) {
					PlayerView playerView = playerViews[i][j];
					WBTeam team = (i == 0) ? _match.getRedTeam() : _match.getWhiteTeam();
					WBPlayer player = team.getPlayerAtNumber(playerView.getPlayerNumber());
					playerView.setTag(player);
					// Set name player
					if(i == 0) {
						player.setName(_redPlayerViews[j].getPlayerName().toString());
						if(_redPlayerViews[j].getPlayerName().toString().equalsIgnoreCase(getString(R.string.default_player_name))) {
							DialogFactory.showSelectPlayerAlert(this, null);
							return;
						}
					} else {
						player.setName(_whitePlayerViews[j].getPlayerName().toString());
						if(_whitePlayerViews[j].getPlayerName().toString().equalsIgnoreCase(getString(R.string.default_player_name))) {
							DialogFactory.showSelectPlayerAlert(this, null);
							return;
						}
					}
				}
			}
			setGameState(GameState.Setupping);
			_duration = 0;
			_hitChain.clear();
	
			// update player views
			updateViewsAfterTurn();
		} else {
			DialogFactory.showSelectTeamAlert(this, null);
		}
	}

	public void onOpenGameClicked(View view) {
		Intent i = new Intent(this, FileActivity.class);
		i.putExtra(EXTRA_SELECT_FILE, true);

		startActivityForResult(i, REQUEST_CODE_SELECT);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == REQUEST_CODE_SELECT && resultCode == RESULT_OK) {
			try {
				//thuat_
				TextView label_match_time = (TextView)findViewById(R.id.label_match_time);
				label_match_time.setVisibility(View.INVISIBLE);
				TextView text_match_time = (TextView)findViewById(R.id.text_match_time);
				text_match_time.setVisibility(View.INVISIBLE);
				
				_match = WBMatch.openFromFile(data.getStringExtra(EXTRA_SELECT_FILE));
				setGameState(GameState.Opening);
				setupPlayerViewsForBallCount(_match.getNumberOfPlayers());
				_hitChain.clear();
				
				//thuat_
				
				red_team_name.setText(_match.getRedTeam().getName());
				white_team_name.setText(_match.getWhiteTeam().getName());
				
				//TODO:
				((ToggleButton) findViewById(R.id.toggle_white_first)).setChecked(_match.isWhiteFirst());
				_isWhiteFirst = _match.isWhiteFirst();
				
				((TextView) findViewById(R.id.text_red_fault_turn)).setText(String.valueOf(_match.getRedTeam().getTeamFault()));
				((TextView) findViewById(R.id.text_white_fault_turn)).setText(String.valueOf(_match.getWhiteTeam().getTeamFault()));
				
				// binding player & player views
				PlayerView playerViews[][] = new PlayerView[2][];
				playerViews[0] = _redPlayerViews;
				playerViews[1] = _whitePlayerViews;
				
				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < _ballNumber; j++) {
						PlayerView playerView = playerViews[i][j];
						WBTeam team = (i == 0) ? _match.getRedTeam() : _match.getWhiteTeam();
						WBPlayer player = team.getPlayerAtNumber(playerView.getPlayerNumber());
						playerView.setTag(player);
						playerView.setPlayerName(player.getName());
					}
				}

				// update player views
				_match.setCurrentTurnIndex(0);
				updateViewsAfterTurn();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void onCloseGameClicked(View view) {
		if (_match != null & _match.getWinnerTeam() == TeamColor.Undefined)
		{
			AlertDialog alert = DialogFactory.getQuestionDialog(this,
					getString(R.string.title_notice),
					getString(R.string.message_close_match), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							setGameState(GameState.Closed);
							clearCount();
							name_red_team = "";
							name_white_team = "";
						}
					}, null);
			alert.show();
		}
		else
		{
			setGameState(GameState.Closed);
			clearCount();
		}
	}

	@SuppressLint("SimpleDateFormat") 
	public void onSaveGameClicked(View view) {
		//Get date and format
		DateFormat dateFormatter = new SimpleDateFormat("MM_dd_yyyy___HH_mm");
		dateFormatter.setLenient(false);
		Date today = new Date();
		String s = dateFormatter.format(today);
		if(!name_red_team.equals("")) {
			txt_red = "___" + name_red_team;
		} else {
			txt_red = "";
		}
		if(!name_white_team.equals("")) {
			txt_white = "___" + name_white_team;
		} else {
			txt_white = "";
		}
		final String txt_dialog = s + txt_red + txt_white;
		String txt_show_dialog = textDialog();
		DialogFactory.getInputTextDialogSave(this, R.string.title_input_file, "",
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				EditText editText = (EditText) ((AlertDialog) dialog)
						.findViewById(R.id.edit_name);
				String fileName = txt_dialog;

				String path = Utils.getDataFolder() + "/" + fileName;
				File file = new File(path);

				if (file.exists()) {
					DialogFactory.showWrongFileName(MainActivity.this, fileName, null);
				} else {
					try {
						String mLang = ((Button) findViewById(R.id.button_language)).getText().toString();
						_match.saveToFile(path, txt_red, txt_white, mLang);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}, null, txt_dialog, txt_show_dialog).show();
	}

	public void onLuckyTeamClicked(View view) {
		Random random = new Random();
		final TeamColor luckyTeam = random.nextBoolean() ? TeamColor.Red : TeamColor.White;
		DialogFactory.showLuckyTeamAlert(this, luckyTeam, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ToggleButton toggleWhiteFirst = (ToggleButton) findViewById(R.id.toggle_white_first);
				toggleWhiteFirst.setChecked(luckyTeam == TeamColor.White);
			}
		});
	}
	
	private String textDialog()
	{
		DateFormat dateFormatter = new SimpleDateFormat("MM-dd-yyyy HH:mm");
		dateFormatter.setLenient(false);
		Date today = new Date();
		String s = dateFormatter.format(today);
		if(!name_red_team.equals("")) {
			txt_red = "[" + name_red_team + "] ";
		} else {
			txt_red = "";
		}
		if(!name_white_team.equals("")) {
			txt_white = " [" + name_white_team + "]";
		} else {
			txt_white = "";
		}
		String txt_dialog = txt_red + "vs" + txt_white + " " + s; 
		return txt_dialog;
	}

	/*
	 * Turn Controls
	 */
	public void onTimeMinusClicked(View view) {
		TextView tv = (TextView) findViewById(R.id.text_count);
		int curVal = Integer.valueOf(tv.getText().toString());
		curVal -= DEFAULT_TIME_STEP;

		tv.setText(String.valueOf(curVal));
		_settings.setCountDown(curVal);
		findViewById(R.id.button_time_minus).setEnabled(curVal > MIN_COUNT);
	}

	public void onTimePlusClicked(View view) {
		TextView tv = (TextView) findViewById(R.id.text_count);
		int curVal = Integer.valueOf(tv.getText().toString());
		curVal += DEFAULT_TIME_STEP;
		tv.setText(String.valueOf(curVal));
		_settings.setCountDown(curVal);
		findViewById(R.id.button_time_minus).setEnabled(curVal > MIN_COUNT);
	}

	public void onChangeTurnClicked(View view) {
		if (_gameState != GameState.Playing)
			return;
		
		if (_hitChain.size() < 1) {
			DialogFactory.showInvalidMoveAlert(this, null);
			return;
		}

		playClickButtonSound();
		WBTurn turn = new WBTurn(_hitChain.get(0), TurnType.Empty);
		_match.addTurn(turn);

		clearCount();

		updateViewsAfterTurn();
		_hitChain.clear();
	}

	private void clearCount() {
		// clear count
		_countDownHandler.removeMessages(0);
		_count = 0;

		((TextView) findViewById(R.id.text_count))
				.setText(String.valueOf(_settings.getCountDown()));
	}

	public void onSkipTurnClicked(View view) {
		if (_gameState != GameState.Playing)
			return;

		if (_hitChain.size() < 1) {
			DialogFactory.showInvalidMoveAlert(this, null);
			return;
		}

		
		clearCount();

		WBTurn turn = new WBTurn(_hitChain.get(0), TurnType.Skip);
		_match.addTurn(turn);

	    if (turn.starter.getTeam().getSkipTurn() == 3) {
	        playWarningSound();
	    } else {
	    	playClickButtonSound();
	    }
		
		updateViewsAfterTurn();
		_hitChain.clear();
	}

	public void onHitTurnClicked(View view) {
		if (_gameState != GameState.Playing)
			return;

		if (_match.getWinnerTeam() != TeamColor.Undefined) {
			DialogFactory.showGameOverAlert(this, null);
			return;
		}

		if (_hitChain.size() < 2) {
			DialogFactory.showInvalidMoveAlert(this, null);
			return;
		}

		playClickButtonSound();
		clearCount();

		WBTurn turn = new WBTurn(_hitChain.get(0), TurnType.Hit);
		turn.hitChain.addAll(_hitChain.subList(1, _hitChain.size()));
		if(_hitChain.get(0).getFoul2()) {
			_hitChain.get(1).setIsFoul2(true);
		}
		_match.addTurn(turn);

		updateViewsAfterTurn();
		_hitChain.get(1).setIsFoul2(false);
		_hitChain.get(0).setFoul2(false);
		_hitChain.clear();

		if (_match.getWinnerTeam() != TeamColor.Undefined) {
			DialogFactory.showWinnerAlert(this, _match.getWinnerTeam(), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					onSaveGameClicked(null);
				}
				
			}, null);
		}
	}
	
	public void onIndivTurn2Clicked(View view) {
		if (_gameState != GameState.Playing)
			return;
		
		if (_hitChain.size() < 1) {
			DialogFactory.showInvalidMoveAlert(this, null);
			return;
		}
		
		playClickButtonSound();
		clearCount();
		
		WBTurn turn = new WBTurn(_hitChain.get(0), TurnType.Error);
		_match.addTurn(turn);
		
		updateViewsAfterTurn();
		_hitChain.clear();
	}

	public void onIndivTurnClicked(View view) {
		if (_gameState != GameState.Playing)
			return;

		if (_hitChain.size() < 1) {
			DialogFactory.showInvalidMoveAlert(this, null);
			return;
		}
		
		playClickButtonSound();
		clearCount();

		WBTurn turn = new WBTurn(_hitChain.get(0), TurnType.Indiv);
		_match.addTurn(turn);

		updateViewsAfterTurn();
		_hitChain.clear();
	}

	public void onBackClicked(View view) {
		if (_match == null)
			return;

		_match.backTurn();

		if (_match.getCurrentTurnIndex() < _ballNumber * 2)
			_gameState = GameState.Setupping;

		updateViewsAfterTurn();
		clearCount();
	}

	public void onNextClicked(View view) {
		if (_match == null)
			return;

		_match.nextTurn();

		if (_match.getCurrentTurnIndex() >= _ballNumber * 2)
			_gameState = GameState.Playing;

		updateViewsAfterTurn();

		clearCount();
	}

	public void onCountClicked(View view) {
		if (_gameState != GameState.Playing)
			return;

		if (_count == 0) {
			// start count
			_count = _settings.getCountDown();
			_countDownHandler.sendEmptyMessageDelayed(0, 1000);
		} else
			clearCount();
	}

	public void onMissTurnClicked(View view) {
		if (_gameState != GameState.Playing)
			return;

		if (_hitChain.size() < 1) {
			DialogFactory.showInvalidMoveAlert(this, null);
			return;
		}

		playClickButtonSound();
		clearCount();

		WBTurn turn = new WBTurn(_hitChain.get(0), TurnType.Miss);
		_match.addTurn(turn);

		updateViewsAfterTurn();
		_hitChain.clear();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
		switch (parent.getId()) {
		case R.id.spinner_ball_number:
			// change number of players
			// pos = 0 -> balls = 5
			// pos = 1 -> balls = 6
			// pos = 2 -> balls = 7
			_ballNumber = pos + 5;
			setupPlayerViewsForBallCount(_ballNumber);
			_settings.setBallCount(_ballNumber);
			break;
		case R.id.spinner_setup:
			if (_gameState != GameState.Setupping)
				return;

			PlayerView playerView = (PlayerView) parent.getTag();
			WBPlayer player = (WBPlayer) playerView.getTag();

			WBTurn turn = new WBTurn(player, TurnType.Setup);
			turn.setupState = playerView.getSetupState();

			if (turn.setupState == SetupState.Attack) 
				playAttackSound();
			else if (turn.setupState == SetupState.Defence)
				playDefenseSound();
			
			_match.addTurn(turn);

			// TODO: not hard code
			if (player.getTeam().getNumberOfActacks() > _ballNumber - 2) {
				DialogFactory.showInvalidSetupAlert(this, SetupState.Attack, null);
				playerView.setSetupState(SetupState.Undefined);
				_match.backTurn();
			} else if (player.getTeam().getNumberOfDefences() > _ballNumber - 2) {
				DialogFactory.showInvalidSetupAlert(this, SetupState.Defence, null);
				playerView.setSetupState(SetupState.Undefined);
				_match.backTurn();
			} else {
				if (_match.isSetupComplete()) {
					setGameState(GameState.Playing);
				}

				updateViewsAfterTurn();
			}

			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	/**
	 * Allow match setting controls enabled or not.
	 * 
	* @param enabled
	 */
	//thuat_
	private void setMatchSettingEnable(boolean enabled) {
		//findViewById(R.id.spinner_ball_number).setEnabled(enabled);
		//findViewById(R.id.toggle_white_first).setEnabled(enabled);
		//btn_select_red.setEnabled(enabled);
		//btn_select_white.setEnabled(enabled);
		
	}

	private void setTurnControlsEnable(boolean enabled) {
		findViewById(R.id.button_indiv_turn2).setEnabled(enabled);
		findViewById(R.id.button_change_turn).setEnabled(enabled);
		findViewById(R.id.button_hit_turn).setEnabled(enabled);
		findViewById(R.id.button_miss_turn).setEnabled(enabled);
		findViewById(R.id.button_skip_turn).setEnabled(enabled);
		findViewById(R.id.button_indiv_turn).setEnabled(enabled);
		findViewById(R.id.button_count).setEnabled(enabled);

		findViewById(R.id.button_back).setEnabled(enabled);
		findViewById(R.id.button_next).setEnabled(enabled);

		findViewById(R.id.button_red_captute_zone_minus).setEnabled(enabled);
		findViewById(R.id.button_red_captute_zone_plus).setEnabled(enabled);
		findViewById(R.id.button_red_fault_turn_minus).setEnabled(enabled);
		findViewById(R.id.button_red_fault_turn_plus).setEnabled(enabled);

		findViewById(R.id.button_white_captute_zone_minus).setEnabled(enabled);
		findViewById(R.id.button_white_captute_zone_plus).setEnabled(enabled);
		findViewById(R.id.button_white_fault_turn_minus).setEnabled(enabled);
		findViewById(R.id.button_white_fault_turn_plus).setEnabled(enabled);
		
	
	}

	private void setGameState(GameState gameState) {
		_gameState = gameState;

		switch (gameState) {
		case Setupping:
			findViewById(R.id.button_new_game).setEnabled(false);
			findViewById(R.id.button_open_game).setEnabled(false);
			findViewById(R.id.button_close_game).setEnabled(true);
			findViewById(R.id.button_lucky_team).setEnabled(false);
			findViewById(R.id.button_save_game).setEnabled(true);
			setMatchSettingEnable(false);
			setTurnControlsEnable(false);
			((TextView) findViewById(R.id.label_number_of_players)).setVisibility(View.INVISIBLE);
			((TextView) findViewById(R.id.label_white_first)).setVisibility(View.INVISIBLE);
			((Spinner) findViewById(R.id.spinner_ball_number)).setVisibility(View.INVISIBLE);
			((ToggleButton) findViewById(R.id.toggle_white_first)).setVisibility(View.INVISIBLE);
			btn_select_red.setVisibility(View.INVISIBLE);
			btn_select_white.setVisibility(View.INVISIBLE);
			break;
		case Opening:
			findViewById(R.id.button_new_game).setEnabled(false);
			findViewById(R.id.button_open_game).setEnabled(false);
			findViewById(R.id.button_close_game).setEnabled(true);
			findViewById(R.id.button_lucky_team).setEnabled(false);
			findViewById(R.id.button_save_game).setEnabled(false);
			setMatchSettingEnable(false);
			setTurnControlsEnable(false);
			((TextView) findViewById(R.id.label_number_of_players)).setVisibility(View.INVISIBLE);
			((TextView) findViewById(R.id.label_white_first)).setVisibility(View.INVISIBLE);
			((Spinner) findViewById(R.id.spinner_ball_number)).setVisibility(View.INVISIBLE);
			((ToggleButton) findViewById(R.id.toggle_white_first)).setVisibility(View.INVISIBLE);
			btn_select_red.setVisibility(View.INVISIBLE);
			btn_select_white.setVisibility(View.INVISIBLE);
			break;
		case Playing:
			findViewById(R.id.button_new_game).setEnabled(false);
			findViewById(R.id.button_open_game).setEnabled(false);
			findViewById(R.id.button_close_game).setEnabled(true);
			findViewById(R.id.button_lucky_team).setEnabled(false);
			findViewById(R.id.button_save_game).setEnabled(true);
			setMatchSettingEnable(false);
			setTurnControlsEnable(true);
			((TextView) findViewById(R.id.label_number_of_players)).setVisibility(View.INVISIBLE);
			((TextView) findViewById(R.id.label_white_first)).setVisibility(View.INVISIBLE);
			((Spinner) findViewById(R.id.spinner_ball_number)).setVisibility(View.INVISIBLE);
			((ToggleButton) findViewById(R.id.toggle_white_first)).setVisibility(View.INVISIBLE);
			btn_select_red.setVisibility(View.INVISIBLE);
			btn_select_white.setVisibility(View.INVISIBLE);
			break;
		case Closed:
			findViewById(R.id.button_new_game).setEnabled(true);
			findViewById(R.id.button_open_game).setEnabled(true);
			findViewById(R.id.button_close_game).setEnabled(false);
			findViewById(R.id.button_lucky_team).setEnabled(true);
			findViewById(R.id.button_save_game).setEnabled(false);
			setMatchSettingEnable(true);
			setTurnControlsEnable(false);
			((TextView) findViewById(R.id.label_number_of_players)).setVisibility(View.VISIBLE);
			((TextView) findViewById(R.id.label_white_first)).setVisibility(View.VISIBLE);
			((Spinner) findViewById(R.id.spinner_ball_number)).setVisibility(View.VISIBLE);
			((ToggleButton) findViewById(R.id.toggle_white_first)).setVisibility(View.VISIBLE);
			btn_select_red.setVisibility(View.VISIBLE);
			btn_select_white.setVisibility(View.VISIBLE);
			red_team_name.setText("");
			white_team_name.setText("");
			TextView label_match_time = (TextView)findViewById(R.id.label_match_time);
			label_match_time.setVisibility(View.VISIBLE);
			TextView text_match_time = (TextView)findViewById(R.id.text_match_time);
			text_match_time.setVisibility(View.VISIBLE);
			resetViewsBeforeMatch();
			break;
		default:
			break;
		}
	}




	/*
	 * Bottom View - Button Clicked Processing
	 */

	private void increaseNumber(int textId, int relatedMinusButtonId) {
		TextView tv = (TextView) findViewById(textId);
		int curVal = Integer.valueOf(tv.getText().toString());
		curVal++;
		tv.setText(String.valueOf(curVal));

		findViewById(relatedMinusButtonId).setEnabled(curVal > 0);
	}

	private void decreaseNumber(int textId, int relatedMinusButtonId) {
		TextView tv = (TextView) findViewById(textId);
		int curVal = Integer.valueOf(tv.getText().toString());
		curVal--;

		tv.setText(String.valueOf(curVal));
		findViewById(relatedMinusButtonId).setEnabled(curVal > 0);
	}

	public void onRedFaultTurnMinusClicked(View view) {
		decreaseNumber(R.id.text_red_fault_turn, R.id.button_red_fault_turn_minus);
		
		if (_match != null) {
			TextView tv = (TextView) findViewById(R.id.text_red_fault_turn);
			_match.getRedTeam().setTeamFault(Integer.valueOf(tv.getText().toString()));
		}
	}

	public void onRedFaultTurnPlusClicked(View view) {
		increaseNumber(R.id.text_red_fault_turn, R.id.button_red_fault_turn_minus);
		
		if (_match != null) {
			TextView tv = (TextView) findViewById(R.id.text_red_fault_turn);
			_match.getRedTeam().setTeamFault(Integer.valueOf(tv.getText().toString()));
		}
	}

	public void onWhiteFaultTurnMinusClicked(View view) {
		decreaseNumber(R.id.text_white_fault_turn, R.id.button_white_fault_turn_minus);
		
		if (_match != null) {
			TextView tv = (TextView) findViewById(R.id.text_white_fault_turn);
			_match.getWhiteTeam().setTeamFault(Integer.valueOf(tv.getText().toString()));
		}
	}

	public void onWhiteFaultTurnPlusClicked(View view) {
		increaseNumber(R.id.text_white_fault_turn, R.id.button_white_fault_turn_minus);
		
		if (_match != null) {
			TextView tv = (TextView) findViewById(R.id.text_white_fault_turn);
			_match.getWhiteTeam().setTeamFault(Integer.valueOf(tv.getText().toString()));
		}
	}

	public void onRedCaptuteZoneMinusClicked(View view) {
		decreaseNumber(R.id.text_red_captute_zone, R.id.button_red_captute_zone_minus);
	}

	public void onRedCaptuteZonePlusClicked(View view) {
		increaseNumber(R.id.text_red_captute_zone, R.id.button_red_captute_zone_minus);
	}

	public void onWhiteCaptuteZoneMinusClicked(View view) {
		decreaseNumber(R.id.text_white_captute_zone, R.id.button_white_captute_zone_minus);
	}

	public void onWhiteCaptuteZonePlusClicked(View view) {
		increaseNumber(R.id.text_white_captute_zone, R.id.button_white_captute_zone_minus);
	}
	
	public void onLanguageClicked(View view) {
		int language = _settings.getLanguage();
		switch (language) {
		case SettingController.LANGUAGE_ENGLISH:
			language = SettingController.LANGUAGE_VIETNAMESE;
			if (buttonMore.getText().equals("Less") || buttonMore.getText().equals("更少")) {
				buttonMore.setText("Thu gọn");
			}
			break;
		case SettingController.LANGUAGE_VIETNAMESE:
			language = SettingController.LANGUAGE_CHINESE;
			if (buttonMore.getText().equals("Less") || buttonMore.getText().equals("Thu gọn")) {
				buttonMore.setText("更少");
			}
			break;
		case SettingController.LANGUAGE_CHINESE:
			language = SettingController.LANGUAGE_ENGLISH;
			if (buttonMore.getText().equals("更少") || buttonMore.getText().equals("Thu gọn")) {
				buttonMore.setText("Less");
			}
			break;
		default:
			break;
		}
		_settings.setLanguage(language);
		updateLanguage(Utils.getLocale(language));
	}
	
	public void onAboutClicked(View view) {
		DialogFactory.showInfoDialog(this, _versionCode);
	}
	
	private void resetText() {
		TextView textRedBall = (TextView) findViewById(R.id.text_red_ball);
		TextView textWhiteBall  = (TextView) findViewById(R.id.text_white_ball);
		String mLang = ((Button) findViewById(R.id.button_language)).getText().toString();
		if(!textRedBall.getText().equals("") || !textWhiteBall.getText().equals("")) {
			if(textRedBall.getText().equals("")) {
				textWhiteBall.setText((_match.getDescriptionOfTurn(_match.getLastTurn(), mLang)));
			} else if (textWhiteBall.getText().equals("")) {
				textRedBall.setText((_match.getDescriptionOfTurn(_match.getLastTurn(), mLang)));
			}
		}
		
		TextView textRedBallRescued = (TextView) findViewById(R.id.text_red_ball_rescued);
		TextView textWhiteBallRescued = (TextView) findViewById(R.id.text_white_ball_rescued);
		if(!textRedBallRescued.getText().equals("") || !textWhiteBallRescued.getText().equals("")) {
			if(!textRedBallRescued.getText().equals("")) {
				textRedBallRescued.setText(_match.getBallRescuedText(_match.getRedTeam(), mLang));
			}
			if(!textWhiteBallRescued.getText().equals("")) {
				textWhiteBallRescued.setText(_match.getBallRescuedText(_match.getWhiteTeam(), mLang));
			}
		}
	}
	
	public void updateLanguage(Locale locale) {
		Resources res = Utils.getResourcesForLocale(getResources(), locale);
		
		ToggleButton toggleButton = (ToggleButton) findViewById(R.id.toggle_white_first);
		toggleButton.setTextOn(getString(R.string.label_white));
		toggleButton.setTextOff(getString(R.string.label_red));

		if (locale == Locale.US)
			((Button) findViewById(R.id.button_language)).setText(R.string.label_en);
		else if (locale == Locale.CHINESE)
			((Button) findViewById(R.id.button_language)).setText(R.string.label_zh);
		else
			((Button) findViewById(R.id.button_language)).setText(R.string.label_vn);
		// Set text toggle button
			if(locale.toString().equals("zh")) {
				resetText();
				if(toggleButton.isChecked()) {
					toggleButton.setText("白");
				} else {
					toggleButton.setText("紅");
				}
			} else if(locale.toString().equals("vi")) {
				resetText();
				if(toggleButton.isChecked()) {
					toggleButton.setText("Trắng");
				} else {
					toggleButton.setText("Đỏ");
				}
			} else {
				resetText();
				if(toggleButton.isChecked()) {
					toggleButton.setText("W");
				} else {
					toggleButton.setText("R");
				}
			}
		//button select team
		
		btn_select_red.setText(res.getString(R.string.label_select_team));
		btn_select_white.setText(res.getString(R.string.label_select_team));
		
//		((TextView) findViewById(R.id.btnFileView)).setText(res.getString(R.string.item_files));
//		((TextView) findViewById(R.id.btnTeamView)).setText(res.getString(R.string.item_teams));
		
		((TextView) findViewById(R.id.label_time)).setText(res.getString(R.string.label_time));
		((TextView) findViewById(R.id.label_match_time)).setText(res.getString(R.string.label_match_time));
		((TextView) findViewById(R.id.label_number_of_players)).setText(res.getString(R.string.label_balls));
		((TextView) findViewById(R.id.label_white_first)).setText(res.getString(R.string.label_white_first));

		((Button) findViewById(R.id.button_new_game)).setText(res.getString(R.string.label_new_game));
		((Button) findViewById(R.id.button_save_game)).setText(res.getString(R.string.label_save_game));
		((Button) findViewById(R.id.button_open_game)).setText(res.getString(R.string.label_open_game));
		((Button) findViewById(R.id.button_close_game)).setText(res.getString(R.string.label_close_game));
		((Button) findViewById(R.id.button_lucky_team)).setText(res.getString(R.string.label_lucky_team));
		
		((TextView) findViewById(R.id.label_red_ball)).setText(res.getString(R.string.label_ball));
		((TextView) findViewById(R.id.label_white_ball)).setText(res.getString(R.string.label_ball));
		((TextView) findViewById(R.id.label_red_ball_rescued)).setText(res.getString(R.string.label_ball_rescued, getString(R.string.label_red_name)));
		((TextView) findViewById(R.id.label_white_ball_rescued)).setText(res.getString(R.string.label_ball_rescued, getString(R.string.label_white_name)));
		
		((Button) findViewById(R.id.button_indiv_turn2)).setText(res.getString(R.string.label_indiv_turn2));
		((Button) findViewById(R.id.button_indiv_turn)).setText(res.getString(R.string.label_indiv_turn));
		((Button) findViewById(R.id.button_hit_turn)).setText(res.getString(R.string.label_hit_turn));
		((Button) findViewById(R.id.button_miss_turn)).setText(res.getString(R.string.label_miss_turn));
		((Button) findViewById(R.id.button_skip_turn)).setText(res.getString(R.string.label_skip_turn));
		((Button) findViewById(R.id.button_change_turn)).setText(res.getString(R.string.label_change_turn));
		
		((TextView) findViewById(R.id.label_current_turn)).setText(res.getString(R.string.label_current_turn));
		((Button) findViewById(R.id.button_count)).setText(res.getString(R.string.label_count));
		((Button) findViewById(R.id.button_next)).setText(res.getString(R.string.label_next));
		((Button) findViewById(R.id.button_back)).setText(res.getString(R.string.label_back));
		
		((TextView) findViewById(R.id.label_red_fault_turn)).setText(res.getString(R.string.label_red_fault_turn));
		((TextView) findViewById(R.id.label_white_fault_turn)).setText(res.getString(R.string.label_white_fault_turn));
		((TextView) findViewById(R.id.label_red_captute_zone)).setText(res.getString(R.string.label_red_captute_zone));
		((TextView) findViewById(R.id.label_white_captute_zone)).setText(res.getString(R.string.label_white_captute_zone));
		
		View redTeamLayout = findViewById(R.id.layout_red_team);
		((TextView) redTeamLayout.findViewById(R.id.label_skip_turn_brief)).setText(res.getString(R.string.label_skip_turn_brief));
		((TextView) redTeamLayout.findViewById(R.id.label_skip_turn_fault_brief)).setText(res.getString(R.string.label_skip_turn_fault_brief));
		((TextView) redTeamLayout.findViewById(R.id.label_score_red)).setText(res.getString(R.string.label_score));
		
		View whiteTeamLayout = findViewById(R.id.layout_white_team);
		((TextView) whiteTeamLayout.findViewById(R.id.label_skip_turn_brief)).setText(res.getString(R.string.label_skip_turn_brief));
		((TextView) whiteTeamLayout.findViewById(R.id.label_skip_turn_fault_brief)).setText(res.getString(R.string.label_skip_turn_fault_brief));
		((TextView) whiteTeamLayout.findViewById(R.id.label_score_white)).setText(res.getString(R.string.label_score));
	}
	
	void playDefenseSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.defense);   
        mp.start();
	}
	
	void playAttackSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.attack);   
        mp.start();
	}
	
	void playWarningSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.warning);   
        mp.start();
	}
	
	void playClickButtonSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.button);   
        mp.start();
	}
	
	void playClickBallSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.ball);   
        mp.start();
	}
	
	public void onMoreClicked(View view) {
		setLess(!_isLess);
	}
	
	void setLess(boolean isLess) {
		_isLess = isLess;
		red_team_name.setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
		white_team_name.setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);

		findViewById(R.id.label_red_fault_turn).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
		findViewById(R.id.label_white_fault_turn).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_red_fault_turn).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_white_fault_turn).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_red_fault_turn_plus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_red_fault_turn_minus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_white_fault_turn_plus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_white_fault_turn_minus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.label_red_captute_zone).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.label_white_captute_zone).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_red_captute_zone).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_white_captute_zone).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_red_captute_zone_minus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_red_captute_zone_plus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_white_captute_zone_minus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_white_captute_zone_plus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    
	    findViewById(R.id.button_language).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_about).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.label_time).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.label_match_time).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_time).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_match_time).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    
	    for (int i = 0; i < _ballNumber; i++) {
	        	_redPlayerViews[i].setNameVisible(!isLess);
	        	_whitePlayerViews[i].setNameVisible(!isLess);
	    }
	    
	    buttonMore = (Button) findViewById(R.id.button_more);
	    
	    if (isLess)
	    	buttonMore.setText(R.string.label_more);
	    else
	    	buttonMore.setText(R.string.label_less);
	    
	    
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		SharedPreferences p = PreferenceManager
				.getDefaultSharedPreferences(this);
		long id = p.getLong("selectTeam", 0);
		if (isSelectTeam != 0) {
			if (id != 0) {
				if (isSelectTeam == 1) {
					// red
					TeamContract teamRed = _dbHelper.getTeamById(id);
					name_red_team = teamRed.name;
					//thuat_
					red_team_name.setText(teamRed.name);

					List<PlayerContract> players = _dbHelper
							.getAllPlayersForTeam(id);
					for (int i = 0; i < 7; i++) {
						int playerNumber = _redPlayerViews[i].getPlayerNumber();
						if (playerNumber <= players.size() && playerNumber > 0) {
							_redPlayerViews[i].setPlayerName(players
									.get(playerNumber - 1).name);
							_redPlayerViews[i]
									.setRole(players.get(playerNumber - 1).role == 1 ? PlayerRole.Captain
											: PlayerRole.Normal);
						} else {
							_redPlayerViews[i]
									.setPlayerName(getString(R.string.default_player_name));
							_redPlayerViews[i].setRole(PlayerRole.Normal);
						}
					}

				} else {
					// white
					TeamContract whiteTeam = _dbHelper.getTeamById(id);
					name_white_team = whiteTeam.name;
					//1385
					white_team_name.setText(whiteTeam.name);

					List<PlayerContract> players = _dbHelper
							.getAllPlayersForTeam(id);
					for (int i = 0; i < 7; i++) {
						int playerNumber = _whitePlayerViews[i]
								.getPlayerNumber();
						if (playerNumber <= players.size() && playerNumber > 0) {
							_whitePlayerViews[i].setPlayerName(players
									.get(playerNumber - 1).name);
							_whitePlayerViews[i]
									.setRole(players.get(playerNumber - 1).role == 1 ? PlayerRole.Captain
											: PlayerRole.Normal);
						} else {
							_whitePlayerViews[i]
									.setPlayerName(getString(R.string.default_player_name));
							_whitePlayerViews[i].setRole(PlayerRole.Normal);
						}
					}
				}

			}

		}
		Editor editor = p.edit();
		editor.putLong("selectTeam", 0);
		editor.commit();
		isSelectTeam = 0;
	}
	
	
	public void openFileView(View view) {
		Intent fileIntent = new Intent(this, FileActivity.class);
		startActivity(fileIntent);
	}
	
	public void openTeamView(View view) {
		Intent teamIntent = new Intent(this, TeamActivity.class);
		startActivity(teamIntent);
	}
	
	
	
}
