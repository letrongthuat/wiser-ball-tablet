package com.goodapps.wiserball;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.goodapps.wiserball.data.DbHelper;
import com.goodapps.wiserball.data.PlayerContract;
import com.goodapps.wiserball.data.PlayerContract.PlayerEntry;
import com.goodapps.wiserball.data.TeamContract;
import com.mobeta.android.dslv.DragSortListView;
import com.mobeta.android.dslv.DragSortListView.DropListener;

public class TeamActivity extends Activity {
	
	private long mSelectdItem = 0;
	//thuatole
	private Button btn_select, btn_cancel;
	private long _id = 0;
	boolean i ;
	private OnItemClickListener _OnPlayerItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			_actionMode = TeamActivity.this.startActionMode(new MyCallback(R.menu.menu_player, id));
			view.setSelected(true);
		}
	};

	private OnItemClickListener _onTeamItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
			viewTeam(id);
			_actionMode = TeamActivity.this.startActionMode(new MyCallback(R.menu.menu_player, id));
			//thuatole
			_id = id;
			if(i == true){
			
			btn_cancel.setVisibility(View.VISIBLE);
			btn_select.setVisibility(View.VISIBLE);
			}
		}
	};

	class MyCallback implements ActionMode.Callback {
		// private ActionMode.Callback _teamActionModeCallback = new
		// ActionMode.Callback() {
		private long _itemId;
		private int _menuResourceId;

		public MyCallback(int menuResourceId, long itemId) {
			_itemId = itemId;
			mSelectdItem = itemId;
			_menuResourceId = menuResourceId;
		}

		// Called when the action mode is created; startActionMode() was called
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// Inflate a menu resource providing context menu items
//			MenuInflater inflater = mode.getMenuInflater();
//			inflater.inflate(_menuResourceId, menu);
			return false;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			return false;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			
		}
	};

	private ActionMode _actionMode;
	private DbHelper _dbHelper;
	private CursorAdapter _teamAdapter;
	private CursorAdapter _playerAdapter;
	private long _currentTeam = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_team);
		btn_select = (Button)findViewById(R.id.button_select_team);
		btn_cancel = (Button)findViewById(R.id.button_cancel_team);
		//thuatole
		SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
		long id = p.getLong("selectTeam", 0);
		if(id == 0)
		{
			btn_cancel.setVisibility(View.GONE);
			btn_select.setVisibility(View.GONE);
		}
		Intent intent = getIntent();
		Bundle isButtonTeamClick = intent.getExtras();
		if(isButtonTeamClick != null){
			if(isButtonTeamClick.containsKey("isButtonSelectTeam")){
			i = isButtonTeamClick.getBoolean("isButtonSelectTeam", false);
			}
		}
		_dbHelper = new DbHelper(this);

		ListView teamList = (ListView) findViewById(R.id.list_team);
		Cursor teamCursor = _dbHelper.getAllTeamsCursor();

		_teamAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1,
				teamCursor, new String[] { TeamContract.TeamEntry.COLUMN_TEAM_NAME },
				new int[] { android.R.id.text1 },
				SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		teamList.setAdapter(_teamAdapter);
		teamList.setOnItemClickListener(_onTeamItemClickListener);

		DragSortListView playerList = (DragSortListView) findViewById(R.id.list_player);
		Cursor playerCursor = _dbHelper.getAllPlayersForTeamCursor(-1);

		_playerAdapter = new SimpleCursorAdapter(this, R.layout.list_item_handle_right,
				playerCursor, new String[] { PlayerEntry.COLUMN_PLAYER_NAME },
				new int[] { android.R.id.text1 },
				SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View v = super.getView(position, convertView, parent);

				long id = getItemId(position);
				PlayerContract player = _dbHelper.getPlayerById(id);
				v.findViewById(R.id.image_role).setVisibility(
						player.role == 1 ? View.VISIBLE : View.INVISIBLE);

				return v;

			}
		};
		playerList.setAdapter(_playerAdapter);
		playerList.setOnItemClickListener(_OnPlayerItemClickListener);

		playerList.setDropListener(new DropListener() {
			@Override
			public void drop(int from, int to) {
				resortPlayer(from, to);
				_playerAdapter.changeCursor(_dbHelper.getAllPlayersForTeamCursor(_currentTeam));
				_playerAdapter.notifyDataSetChanged();
			}
		});
	}

	private void resortPlayer(int from, int to) {
		long playerId;
		PlayerContract player;
		if (from < to) {
			int run = from + 1;
			for (; run <= to; run++) {
				playerId = _playerAdapter.getItemId(run);
				player = _dbHelper.getPlayerById(playerId);
				player.order--;
				_dbHelper.updatePlayer(player);
			}
			playerId = _playerAdapter.getItemId(from);
			player = _dbHelper.getPlayerById(playerId);
			player.order = to;
			_dbHelper.updatePlayer(player);
		} else if (from > to) {
			int run = from - 1;
			for (; run >= to; run--) {
				playerId = _playerAdapter.getItemId(run);
				player = _dbHelper.getPlayerById(playerId);
				player.order++;
				_dbHelper.updatePlayer(player);
			}
			playerId = _playerAdapter.getItemId(from);
			player = _dbHelper.getPlayerById(playerId);
			player.order = to;
			_dbHelper.updatePlayer(player);
		}
	}

	private void viewTeam(long id) {
		_currentTeam = id;
		_playerAdapter.changeCursor(_dbHelper.getAllPlayersForTeamCursor(id));
		_playerAdapter.notifyDataSetChanged();

		TeamContract team = _dbHelper.getTeamById(id);
		TextView tv = (TextView) findViewById(R.id.text_title_player);
		if (team == null)
			tv.setText(R.string.label_players);
		else
			tv.setText(getString(R.string.label_players) + " - " + team.name);
	}

	// Action click add team
	public void onAddTeamClicked(View view) {
		AlertDialog inputDialog = DialogFactory.getInputTextDialog(this, R.string.title_input_team,
				"", new OnClickListener() {
					@Override
					public void onClick(DialogInterface parent, int which) {
						Dialog dialog = (Dialog) parent;
						TeamContract team = new TeamContract();
						EditText editText = (EditText) dialog.findViewById(R.id.edit_name);
						team.name = editText.getText().toString();
						_dbHelper.insertTeam(team);
						_teamAdapter.changeCursor(_dbHelper.getAllTeamsCursor());
						_teamAdapter.notifyDataSetChanged();
					}
				}, null);
		inputDialog.show();
	}
	
	// Action click rename team
	public void onRenameTeamClicked(View view) {
		if(mSelectdItem > 0) {
			renameTeam(mSelectdItem);
		}
	}
	
	// Action add edit team
	public void onEditTeamClicked(View view) {
		if(mSelectdItem > 0) {
			askForDeletingTeam(mSelectdItem);
		}
	}
	
	// Action click add player
	public void onAddPlayerClicked(View view) {
		AlertDialog inputDialog = DialogFactory.getInputTextDialog(this,
				R.string.title_input_player, "", new OnClickListener() {
					@Override
					public void onClick(DialogInterface parent, int which) {
						if (_currentTeam != -1) {
							Dialog dialog = (Dialog) parent;
							PlayerContract player = new PlayerContract();
							EditText editText = (EditText) dialog.findViewById(R.id.edit_name);
							player.name = editText.getText().toString();
							player.role = 0;
							player.teamId = _currentTeam;
							player.order = _teamAdapter.getCount();
							_dbHelper.insertPlayer(player);
							_playerAdapter.changeCursor(_dbHelper
									.getAllPlayersForTeamCursor(_currentTeam));
							_playerAdapter.notifyDataSetChanged();
						}
					}
				}, null);
		inputDialog.show();
	}
	
	// Action edit player
	public void onEditPlayerClicked(View view) {
		if(mSelectdItem > 0) {
			askForDeletingPLayer(mSelectdItem);
		}
	}
	
	// Action rename player
	public void onRenamePlayerClicked(View view) {
		if(mSelectdItem > 0) {
			renamePlayer(mSelectdItem);
		}
	}
	
	// Action click captain
	public void onSelectCaptainClicked(View view) {
		setCaptain(mSelectdItem);
	}
	
	//thuatole
		// Onclick selected team 
		public void onSelectTeamClicked(View view) {
			// Finish activity
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			Editor editor = sp.edit();
			editor.putLong("selectTeam", _id);
			editor.commit();
			finish();
			
		}
		
		// Onclick selected team 
			public void onCancelPlayerClicked(View view) {
				// Finish activity
				finish();
				
			}

	private void askForDeletingTeam(final long id) {
		TeamContract team = _dbHelper.getTeamById(id);

		AlertDialog alert = DialogFactory.getQuestionDialog(this,
				getString(R.string.title_delete_team),
				getString(R.string.message_delete_team, team.name), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (id == _currentTeam) {
							viewTeam(-1);
						}
						_dbHelper.deleteTeam(id);
						_teamAdapter.changeCursor(_dbHelper.getAllTeamsCursor());
						_teamAdapter.notifyDataSetChanged();
					}
				}, null);
		alert.show();
	}

	private void renameTeam(final long id) {
		final TeamContract team = _dbHelper.getTeamById(id);

		AlertDialog alert = DialogFactory.getInputTextDialog(this, R.string.title_rename_team,
				team.name, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Dialog d = (Dialog) dialog;
						EditText editText = (EditText) d.findViewById(R.id.edit_name);

						String newName = editText.getText().toString();
						if (newName.trim().length() != 0) {
							team.name = newName;
							_dbHelper.updateTeam(team);
							_teamAdapter.changeCursor(_dbHelper.getAllTeamsCursor());
							_teamAdapter.notifyDataSetChanged();
						}
					}
				}, null);
		alert.show();
	}

	private void renamePlayer(final long id) {
		final PlayerContract player = _dbHelper.getPlayerById(id);

		AlertDialog alert = DialogFactory.getInputTextDialog(this, R.string.title_rename_player,
				player.name, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Dialog d = (Dialog) dialog;
						EditText editText = (EditText) d.findViewById(R.id.edit_name);

						String newName = editText.getText().toString();
						if (newName.trim().length() != 0) {
							player.name = newName;
							_dbHelper.updatePlayer(player);
							_playerAdapter.changeCursor(_dbHelper
									.getAllPlayersForTeamCursor(_currentTeam));
							_playerAdapter.notifyDataSetChanged();
						}
					}
				}, null);
		alert.show();
	}

	private void askForDeletingPLayer(final long id) {
		PlayerContract player = _dbHelper.getPlayerById(id);

		AlertDialog alert = DialogFactory.getQuestionDialog(this,
				getString(R.string.title_delete_player),
				getString(R.string.message_delete_player, player.name), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						_dbHelper.deletePlayer(id);
						_playerAdapter.changeCursor(_dbHelper
								.getAllPlayersForTeamCursor(_currentTeam));
						_playerAdapter.notifyDataSetChanged();
					}
				}, null);
		alert.show();
	}

	private void setCaptain(long captainId) {
		if(_playerAdapter.getCount() > 0) {
			for (int i = 0; i < _playerAdapter.getCount(); i++) {
				if(i == 0) {
					PlayerContract player = _dbHelper.getPlayerById(_playerAdapter.getItemId(i));
					player.role = 1;
					_dbHelper.updatePlayer(player);
				} else {
					PlayerContract player = _dbHelper.getPlayerById(_playerAdapter.getItemId(i));
					player.role = 0;
					_dbHelper.updatePlayer(player);
				}
			}
		}
		_playerAdapter.changeCursor(_dbHelper.getAllPlayersForTeamCursor(_currentTeam));
		_playerAdapter.notifyDataSetChanged();
	}
	public void openFileView(View view) {
		Intent teamIntent = new Intent(this, FileActivity.class);
		startActivity(teamIntent);
	}
	public void openMainView(View view) {
		Intent teamIntent = new Intent(this, MainActivity.class);
		startActivity(teamIntent);
	}
}
