package com.goodapps.wiserball;

import java.util.Locale;

import com.goodapps.wiserball.R.string;

import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Environment;
import android.util.DisplayMetrics;

public class Utils {
	
	public static int index = -1;
	public static String LANG_ENG = "English";
	public static String LANG_VIE = "Tiếng Việt";
	public static String LANG_CHI = "中文";
	
	public static String getDataFolder() {
		return Environment.getExternalStorageDirectory().getPath() + "/WiserBall";
	}
	
	public static Resources getResourcesForLocale(Resources defaultResources, Locale locale) {
		AssetManager assets = defaultResources.getAssets();
		DisplayMetrics metrics = defaultResources.getDisplayMetrics();
		Configuration config = new Configuration(defaultResources.getConfiguration());
		config.locale = locale;
		Resources resources = new Resources(assets, metrics, config);
		
		return resources;
	}
	
	public static Locale getLocale(int languageCode) {
		switch (languageCode) {
		case SettingController.LANGUAGE_VIETNAMESE:
			return new Locale("vi");
		case SettingController.LANGUAGE_CHINESE:
			return Locale.CHINESE;
		default:
			return Locale.US;
		}
	}
}
