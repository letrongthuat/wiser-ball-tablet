package com.goodapps.wiserball.data;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.goodapps.wiserball.data.PlayerContract.PlayerEntry;
import com.goodapps.wiserball.data.TeamContract.TeamEntry;

public class DbHelper extends SQLiteOpenHelper {
	// If you change the database schema, you must increment the database
	// version.
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "WiserBall.db";

	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TeamContract.SQL_CREATE_ENTRIES);
		db.execSQL(PlayerContract.SQL_CREATE_ENTRIES);
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// This database is only a cache for online data, so its upgrade policy
		// is
		// to simply to discard the data and start over
		db.execSQL(TeamContract.SQL_DELETE_ENTRIES);
		db.execSQL(PlayerContract.SQL_DELETE_ENTRIES);
		onCreate(db);
	}

	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}

	public long insertTeam(TeamContract team) {
		// Gets the data repository in write mode
		SQLiteDatabase db = getWritableDatabase();

		// Create a new map of values, where column names are the keys
		ContentValues values = new ContentValues();
		values.put(TeamContract.TeamEntry.COLUMN_TEAM_NAME, team.name);

		// Insert the new row, returning the primary key value of the new row
		long newRowId;
		newRowId = db.insert(TeamEntry.TABLE_NAME, null, values);
		return newRowId;
	}

	public List<TeamContract> getAllTeams() {
		Cursor c = getAllTeamsCursor();

		List<TeamContract> list = new ArrayList<TeamContract>();
		TeamContract team = null;
		while (c.moveToNext()) {
			team = new TeamContract();
			team.id = c.getLong(c.getColumnIndex(TeamEntry._ID));
			team.name = c.getString(c.getColumnIndex(TeamEntry.COLUMN_TEAM_NAME));
			list.add(team);
		}
		
		return list;
	}
	
	public Cursor getAllTeamsCursor() {
		SQLiteDatabase db = getReadableDatabase();

		// Define a projection that specifies which columns from the database
		// you will actually use after this query.
		String[] projection = { TeamEntry._ID, TeamEntry.COLUMN_TEAM_NAME, };

		// How you want the results sorted in the resulting Cursor
		String sortOrder = TeamEntry.COLUMN_TEAM_NAME + " ASC";

		Cursor c = db.query(TeamEntry.TABLE_NAME, // The table to query
				projection, // The columns to return
				null, // The columns for the WHERE clause
				null, // The values for the WHERE clause
				null, // don't group the rows
				null, // don't filter by row groups
				sortOrder // The sort order
				);

		return c;
	}

	public void deleteTeam(long teamId) {
		SQLiteDatabase db = getWritableDatabase();
		// Define 'where' part of query.
		String selection = TeamEntry._ID + " = ?";
		// Specify arguments in placeholder order.
		String[] selectionArgs = { String.valueOf(teamId) };
		// Issue SQL statement.
		db.delete(TeamEntry.TABLE_NAME, selection, selectionArgs);
		db.close();
	}

	public void updateTeam(TeamContract team) {
		SQLiteDatabase db = getReadableDatabase();

		// New value for one column
		ContentValues values = new ContentValues();
		values.put(TeamEntry.COLUMN_TEAM_NAME, team.name);

		// Which row to update, based on the ID
		String selection = TeamEntry._ID + " = ?";
		String[] selectionArgs = { String.valueOf(team.id) };

		db.update(TeamEntry.TABLE_NAME, values, selection, selectionArgs);
		db.close();
	}

	public TeamContract getTeamById(long id) {
		SQLiteDatabase db = getReadableDatabase();

		// Define a projection that specifies which columns from the database
		// you will actually use after this query.
		String[] projection = { TeamEntry._ID, TeamEntry.COLUMN_TEAM_NAME, };

		Cursor c = db.query(TeamEntry.TABLE_NAME, // The table to query
				projection, // The columns to return
				TeamEntry._ID + "=?", // The columns for the WHERE clause
				new String[] { String.valueOf(id) }, // The values for the WHERE
														// clause
				null, // don't group the rows
				null, // don't filter by row groups
				null // The sort order
				);

		TeamContract team = null;
		if (c.moveToFirst()) {
			team = new TeamContract();
			team.id = id;
			team.name = c.getString(c.getColumnIndex(TeamEntry.COLUMN_TEAM_NAME));
		}
		c.close();
		return team;
	}

	public PlayerContract getPlayerById(long id) {
		SQLiteDatabase db = getReadableDatabase();

		Cursor c = db.query(PlayerEntry.TABLE_NAME, // The table to query
				null, // The columns to return
				PlayerEntry._ID + "=?", // The columns for the WHERE clause
				new String[] { String.valueOf(id) }, // The values for the WHERE
														// clause
				null, // don't group the rows
				null, // don't filter by row groups
				null // The sort order
				);

		PlayerContract player = null;
		if (c.moveToFirst()) {
			player = new PlayerContract();
			player.id = id;
			player.name = c.getString(c.getColumnIndex(PlayerEntry.COLUMN_PLAYER_NAME));
			player.role = c.getInt(c.getColumnIndex(PlayerEntry.COLUMN_ROLE));
			player.order = c.getInt(c.getColumnIndex(PlayerEntry.COLUMN_ORDER));
			player.teamId = c.getInt(c.getColumnIndex(PlayerEntry.COLUMN_TEAM_ID));
		}
		c.close();
		return player;
	}
	
	public long insertPlayer(PlayerContract player) {
		// Gets the data repository in write mode
		SQLiteDatabase db = getWritableDatabase();

		// Create a new map of values, where column names are the keys
		ContentValues values = new ContentValues();
		values.put(PlayerEntry.COLUMN_PLAYER_NAME, player.name);
		values.put(PlayerEntry.COLUMN_ROLE, player.role);
		values.put(PlayerEntry.COLUMN_ORDER, player.order);
		values.put(PlayerEntry.COLUMN_TEAM_ID, player.teamId);

		// Insert the new row, returning the primary key value of the new row
		long newRowId;
		newRowId = db.insert(PlayerEntry.TABLE_NAME, null, values);
		return newRowId;
	}

	public Cursor getAllPlayersForTeamCursor(long teamId) {
		SQLiteDatabase db = getReadableDatabase();
		String sortOrder = PlayerEntry.COLUMN_ORDER + " ASC";
		Cursor c = db.query(PlayerEntry.TABLE_NAME, // The table to query
				null, // The columns to return
				PlayerEntry.COLUMN_TEAM_ID + "=?", // The columns for the WHERE clause
				new String[] { String.valueOf(teamId) }, // The values for the WHERE
														// clause
				null, // don't group the rows
				null, // don't filter by row groups
				sortOrder // The sort order
				);
		return c;
	}

	public void deletePlayer(long playerId) {
		SQLiteDatabase db = getWritableDatabase();
		String selection = PlayerEntry._ID + " = ?";
		String[] selectionArgs = { String.valueOf(playerId) };
		db.delete(PlayerEntry.TABLE_NAME, selection, selectionArgs);
		db.close();
	}

	public void updatePlayer(PlayerContract player) {
		SQLiteDatabase db = getReadableDatabase();

		ContentValues values = new ContentValues();
		values.put(PlayerEntry.COLUMN_PLAYER_NAME, player.name);
		values.put(PlayerEntry.COLUMN_ROLE, player.role);
		values.put(PlayerEntry.COLUMN_ORDER, player.order);
		values.put(PlayerEntry.COLUMN_TEAM_ID, player.teamId);
		String selection = PlayerEntry._ID + " = ?";
		String[] selectionArgs = { String.valueOf(player.id) };

		db.update(PlayerEntry.TABLE_NAME, values, selection, selectionArgs);
		db.close();
	}

	public List<PlayerContract> getAllPlayersForTeam(long teamId) {
		Cursor c = getAllPlayersForTeamCursor(teamId);

		List<PlayerContract> list = new ArrayList<PlayerContract>();
		PlayerContract player = null;
		while (c.moveToNext()) {
			player = new PlayerContract();
			player.id = c.getLong(c.getColumnIndex(PlayerEntry._ID));
			player.name = c.getString(c.getColumnIndex(PlayerEntry.COLUMN_PLAYER_NAME));
			player.role = c.getInt(c.getColumnIndex(PlayerEntry.COLUMN_ROLE));
			player.order = c.getInt(c.getColumnIndex(PlayerEntry.COLUMN_ORDER));
			player.teamId = c.getInt(c.getColumnIndex(PlayerEntry.COLUMN_TEAM_ID));
			list.add(player);
		}
		
		return list;
	}
}
