package com.goodapps.wiserball.game;

import java.util.LinkedList;
import java.util.List;

import com.goodapps.wiserball.type.SetupState;
import com.goodapps.wiserball.type.TurnType;

public class WBTurn {
	public TurnType turnType;
	public WBPlayer starter;
	public List<WBPlayer> hitChain;
	public SetupState setupState;
	
	public WBTurn(WBPlayer player, TurnType type) {
		hitChain = new LinkedList<WBPlayer>();
		turnType = type;
		starter = player;
	}
	
	public static boolean isValidTurn(WBTurn turn) {
		if (turn == null)
			return false;
		
		if (turn.turnType != TurnType.Empty && turn.starter == null)
			return false;
		
		if (turn.turnType == TurnType.Hit && turn.hitChain.size() == 0)
			return false;
		
		if (turn.turnType == TurnType.Setup && turn.setupState == SetupState.Undefined)
			return false;
		
		return true;
	}
	
	public boolean isHitToTeammates() {
		if (!WBTurn.isValidTurn(this))
			return false;
		
		if (turnType != TurnType.Hit)
			return false;
		
		WBPlayer first = hitChain.get(0);
		if (first.getTeam() == starter.getTeam())
			return true;
		
		return false;
	}
}

