package com.goodapps.wiserball.type;

public enum SetupState {
	Undefined,
	Defence,
	Attack
}
