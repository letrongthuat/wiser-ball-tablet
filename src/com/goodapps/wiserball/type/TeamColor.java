package com.goodapps.wiserball.type;

public enum TeamColor {
	Undefined, Red, White
}
